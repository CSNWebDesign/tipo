//
//  AppDelegate.h
//  Tipo
//
//  Created by Александр on 14.09.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

