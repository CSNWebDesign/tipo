//
//  main.m
//  Tipo
//
//  Created by Александр on 14.09.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
