//
//  NSError+Network.m
//  Tipo
//
//  Created by Александр on 04.10.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import "NSError+Network.h"

@implementation NSError (Network)

+ (NSString *)errorLocalizedDescriptionForCode:(NSInteger)errorCode {
    NSDictionary *codes = @{
                            @"1" : @"User with that email is already exist.",
                            @"2" : @"Password is less that 6 symbols.",
                            @"3" : @"Password is more that 20 symbols.",
                            @"4" : @"Whitespaces not allowed in password.",
                            @"5" : @"Email not in correct format.",
                            @"6" : @"User with that email doesn't exist.",
                            @"7" : @"User with that token doesn't exist.",
                            @"8" : @"Incorrect old password.",
                            @"9" : @"New password is less than 6 symbols.",
                            @"10" : @" New password is more than 20 symbols.",
                            @"11" : @"New password can't contain(s) whitespace(s).",
                            @"12" : @"You enter the same email as you already have.",
                            @"13" : @"This email is already taken by someone.",
                            @"14" : @"Task doesn't exist.",
                            };
    
    return [codes valueForKey:[NSString stringWithFormat:@"%ld", (long)errorCode]];
}

@end
