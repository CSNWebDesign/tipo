//
//  NetworkManager.h
//  Tipo
//
//  Created by Александр on 04.10.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CompletionHandler)(BOOL success, id response, NSError *error);

@interface NetworkManager : NSObject

@property (strong, nonatomic) NSNumber* currentUserID;

+ (NetworkManager *)sharedManager;

#pragma mark - Sign Up

- (void)loginUserWithUserName:(NSString *)emailAddress
                     password:(NSString *)password
                   completion:(CompletionHandler)completion;

- (void)registerUserWithUserName:(NSString *)firstName
                        lastName:(NSString *)lastName
                    contactPhone:(NSString *)contactPhone
                    emailAddress:(NSString *)emailAddress
                        password:(NSString *)password
                 confirmPassword:(NSString *)confirmPassword
                             age:(NSString *)age
                           state:(NSString *)state
                         zipcode:(NSString *)zipcode
                          gender:(NSString *)gender
                       birthdate:(NSString *)birthdate
                      completion:(CompletionHandler)completion;
    
- (void)facebookRegisterUserWithUserName:(NSString *)firstName
                                lastName:(NSString *)lastName
                            emailAddress:(NSString *)emailAddress
                                  userID:(NSString *)userID
                                  gender:(NSString *)gender
                              completion:(CompletionHandler)completion;
    

//- (void)getRestaurantWithID:(NSString *)userID completion:(CompletionHandler)completion;

//- (void)restorePasswordWithEmail:(NSString *)email completion:(CompletionHandler)completion;


#pragma mark - Orders

- (void)getNewOrders:(NSString *)userID completion:(CompletionHandler)completion;

@end
