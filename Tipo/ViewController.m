//
//  ViewController.m
//  Tipo
//
//  Created by Александр on 14.09.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import "ViewController.h"
#import "NetworkManager.h"
#import "MainMapViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)unwindLogin:(UIStoryboardSegue *)unwindSegue {
}


- (IBAction)loginButtonTap:(id)sender {
    
    [[NetworkManager sharedManager] loginUserWithUserName:self.loginTextField.text password:self.passwordTextField.text completion:^(BOOL success, id response, NSError *error) {
        
        if (!error) {
            NSLog(@"resp: %@", response);
            
            [self pushToMapViewController];
            
        } else {
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"isLogedIn"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Tipo"
                                                                           message:error.localizedDescription
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* noAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * action) {}];
            
            [alert addAction:noAction];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }];
}


- (IBAction)signInWithFacebook:(id)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in: %@", result.grantedPermissions);
             
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email,first_name, last_name, gender"}]
              startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                  if (!error) {
                      NSLog(@"%@", result);
                      NSString *userID = [result valueForKey:@"id"];
                      NSString *firstName = [result valueForKey:@"first_name"];
                      NSString *lastName = [result valueForKey:@"last_name"];
                      NSString *email =  [result valueForKey:@"email"];
                      NSString *gender =  [[result valueForKey:@"gender"] isEqualToString:@"male"] ? @"M" : @"F";

                      NSLog(@"%@\n%@\n%@\n%@\n%@\n", userID, firstName, lastName, email, gender);
                      
                      [[NetworkManager sharedManager] facebookRegisterUserWithUserName:firstName lastName:lastName emailAddress:email userID:userID gender:gender completion:^(BOOL success, id response, NSError *error) {
                          
                          if (!error) {
                              NSLog(@"resp: %@", response);
                              
                              [self pushToMapViewController];
                              
                          } else {
                              [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"isLogedIn"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              
                              UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Tipo"
                                                                                             message:error.localizedDescription
                                                                                      preferredStyle:UIAlertControllerStyleAlert];
                              
                              UIAlertAction* noAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                                               handler:^(UIAlertAction * action) {}];
                              
                              [alert addAction:noAction];
                              
                              [self presentViewController:alert animated:YES completion:nil];
                          }

                          
                      }];
                      

                  }
                  else{
                      NSLog(@"%@",error.localizedDescription);
                  }
              }];
         }
     }];
    
}

- (IBAction)registrationButtonTap:(id)sender {
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void)pushToMapViewController {
    
    MainMapViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainMapViewController"];
    [[[UIApplication sharedApplication] delegate].window setRootViewController:loginVC];
    [UIView transitionWithView:[[UIApplication sharedApplication] delegate].window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                        });
                    }
                    completion:^(BOOL finished) {
                        [[NSUserDefaults standardUserDefaults] setObject:@"LogedIN" forKey:@"isLogedIn"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }];
}

@end
