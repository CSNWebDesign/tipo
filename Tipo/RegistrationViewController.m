//
//  RegistrationViewController.m
//  Tipo
//
//  Created by Александр on 15.09.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import "RegistrationViewController.h"
#import "NetworkManager.h"
#import "MainMapViewController.h"


@interface RegistrationViewController ()
@property (weak, nonatomic) IBOutlet UIButton *customGenderSwitcher;
@property (assign, nonatomic) BOOL isMale;


@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthdateTextField;
@property (weak, nonatomic) IBOutlet UITextField *stateTextField;
@property (weak, nonatomic) IBOutlet UITextField *ZipCodeTextField;



@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isMale = YES;
    
    NSLog(@"%@", self.userInfo);
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)registerUserButton:(id)sender {
    
    [[NetworkManager sharedManager] registerUserWithUserName:self.firstName.text lastName:self.lastName.text contactPhone:self.userInfo[@"phone"] emailAddress:self.userInfo[@"email"] password:self.userInfo[@"password"] confirmPassword:self.userInfo[@"confirmPassword"] age:self.ageTextField.text state:self.stateTextField.text zipcode:self.ZipCodeTextField.text gender:self.isMale ? @"M" : @"F" birthdate:self.birthdateTextField.text completion:^(BOOL success, id response, NSError *error) {
        
        if (!error) {
            NSLog(@"resp: %@", response);
            
            [self pushToMapViewController];
            
        } else {
            
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"isLogedIn"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Tipo"
                                                                           message:error.localizedDescription
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* noAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * action) {
                                                                 [self performSegueWithIdentifier:@"unwindToStartRegistration" sender:nil];
                                                             }];
            
            [alert addAction:noAction];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }];
}

- (void)pushToMapViewController {
    
    MainMapViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainMapViewController"];
    [[[UIApplication sharedApplication] delegate].window setRootViewController:loginVC];
    [UIView transitionWithView:[[UIApplication sharedApplication] delegate].window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                        });
                    }
                    completion:^(BOOL finished) {
                        [[NSUserDefaults standardUserDefaults] setObject:@"LogedIN" forKey:@"isLogedIn"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }];
}


- (IBAction)buttonSwitcherGender:(UIButton*)sender {
    
    if (self.isMale) {
        [self.customGenderSwitcher setImage:[UIImage imageNamed:@"switcherRight" ] forState:UIControlStateNormal];
        self.isMale = NO;
    } else {
        [self.customGenderSwitcher setImage:[UIImage imageNamed:@"switcherLeft" ] forState:UIControlStateNormal];
        self.isMale = YES;
    }
    
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
