//
//  PhoneNumberRegistrationViewController.h
//  Tipo
//
//  Created by Александр on 20.09.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneNumberRegistrationViewController : UIViewController <UITextFieldDelegate>

@end
