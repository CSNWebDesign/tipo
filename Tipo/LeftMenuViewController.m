//
//  LeftMenuViewController.m
//  Tipo
//
//  Created by Александр on 04.10.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "ViewController.h"

@interface LeftMenuViewController ()

@end

@implementation LeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)logOutButtonTap:(id)sender {
    [self pushToStartViewController];

}

- (void)pushToStartViewController {
    
    ViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [[[UIApplication sharedApplication] delegate].window setRootViewController:loginVC];
    [UIView transitionWithView:[[UIApplication sharedApplication] delegate].window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                        });
                    }
                    completion:^(BOOL finished) {
                        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"isLogedIn"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
