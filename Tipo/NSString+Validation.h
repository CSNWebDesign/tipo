//
//  NSString+Validation.h
//  Tipo
//
//  Created by Александр on 04.10.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validation)

- (BOOL)isEmptyOrWhiteSpace;
- (BOOL)isEmailValid;
- (BOOL)isHashtagValid;


@end
