//
//  NSError+Network.h
//  Tipo
//
//  Created by Александр on 04.10.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (Network)

+ (NSString *)errorLocalizedDescriptionForCode:(NSInteger)errorCode;

@end
