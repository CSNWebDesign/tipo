//
//  PhoneNumberRegistrationViewController.m
//  Tipo
//
//  Created by Александр on 20.09.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import "PhoneNumberRegistrationViewController.h"
#import "RegistrationViewController.h"

@interface PhoneNumberRegistrationViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nextButtonBottomConstraint;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;

@end

@implementation PhoneNumberRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
//    if (textField.tag == 10) {
//        if (textField.text.length > 14 ) {
//            return NO;
//        }
//        
//        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
//        NSArray *components = [newString componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
//        NSString *decimalString = [components componentsJoinedByString:@""];
//        
//        NSUInteger length = decimalString.length;
//        BOOL hasLeadingOne = length > 0 && [decimalString characterAtIndex:0] == '1';
//        
//        if (length == 0 || (length > 10 && !hasLeadingOne) || (length > 11)) {
//            textField.text = decimalString;
//            return NO;
//        }
//        
//        NSUInteger index = 0;
//        NSMutableString *formattedString = [NSMutableString string];
//        
//        if (hasLeadingOne) {
//            [formattedString appendString:@"1 "];
//            index += 1;
//        }
//        
//        if (length - index > 3) {
//            NSString *areaCode = [decimalString substringWithRange:NSMakeRange(index, 3)];
//            [formattedString appendFormat:@"(%@) ",areaCode];
//            index += 3;
//        }
//        
//        if (length - index > 3) {
//            NSString *prefix = [decimalString substringWithRange:NSMakeRange(index, 3)];
//            [formattedString appendFormat:@"%@-",prefix];
//            index += 3;
//        }
//        
//        NSString *remainder = [decimalString substringFromIndex:index];
//        [formattedString appendString:remainder];
//        
//        textField.text = formattedString;
//        
//        return NO;
//    }
    
    return YES;
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    [UIView animateWithDuration:0.8 animations:^{
        self.nextButtonBottomConstraint.constant = keyboardFrameBeginRect.size.height;
    }];
    
    [self.view layoutIfNeeded];
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.8 animations:^{
        self.nextButtonBottomConstraint.constant = 20;
    }];
    [self.view layoutIfNeeded];

}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)nextButtonTap:(id)sender {
    
    NSDictionary* userInfo = @{@"email":self.emailTextField.text,
                               @"password":self.passwordTextField.text,
                               @"confirmPassword":self.confirmPasswordTextField.text,
                               @"phone":[self getNumbersFromString:self.phoneNumberTextField.text]};
    
    [self performSegueWithIdentifier:@"RegistrationViewController" sender:userInfo];
}


- (IBAction)unwindToStartRegistration:(UIStoryboardSegue *)unwindSegue
{
}

-(NSString*)getNumbersFromString:(NSString*)String{
    NSArray* Array = [String componentsSeparatedByCharactersInSet:
                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
    NSString* returnString = [Array componentsJoinedByString:@""];
    
    return (returnString);
    
}
    
- (IBAction)backButtonTap:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"RegistrationViewController"]) {
        RegistrationViewController *vc = [segue destinationViewController];
        [vc setUserInfo:sender];
    }
    
}


@end
