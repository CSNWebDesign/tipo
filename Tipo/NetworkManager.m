//
//  NetworkManager.m
//  Tipo
//
//  Created by Александр on 04.10.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import "NetworkManager.h"
#import "NetworkManagerConstants.h"
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"
#import "NSError+Network.h"


@interface NetworkManager()

@property (strong, nonatomic) AFHTTPSessionManager *manager;


@end

@implementation NetworkManager

+ (NetworkManager *)sharedManager {
    static NetworkManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[NetworkManager alloc] init];
        
    });
    
    return manager;
}

#pragma mark -  Lifecycle

- (instancetype)init {
    if (self = [super init]) {
        [self initManager];
        [self configureReachabilityManager];
    }
    
    return self;
}

- (void)initManager {
    NSURL *baseURL = [NSURL URLWithString:baseURLString];
    self.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    self.manager.securityPolicy = securityPolicy;
}

#pragma mark - Rechability

- (void)configureReachabilityManager{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
    }];
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

#pragma mark - Private

- (void)perfomRequestWithPath:(NSString *)path
                   parameters:(NSDictionary *)parameters
                   completion:(CompletionHandler)completion {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [self.manager POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *error;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        NSDictionary *responceDict = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
        
        NSLog(@"Responce: %@", responceDict);
        
        if (error) {
            completion(NO, nil, error);
        } else {
            if ([[responceDict valueForKey:@"code"] integerValue] != 1) {
                NSDictionary *userInfo = @{
                                           NSLocalizedDescriptionKey: [responceDict valueForKey:@"msg"]
                                           };
                NSError *error = [NSError errorWithDomain:NSRegistrationDomain
                                                     code:[[responceDict valueForKey:@"code"] intValue]
                                                 userInfo:userInfo];
                completion(NO,nil,error);
            } else {
                completion(YES, responceDict, nil);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        completion(NO, nil, error);
    }];

}


#pragma mark - Sign Up


- (void)registerUserWithUserName:(NSString *)firstName
                        lastName:(NSString *)lastName
                        contactPhone:(NSString *)contactPhone
                        emailAddress:(NSString *)emailAddress
                        password:(NSString *)password
                        confirmPassword:(NSString *)confirmPassword
                        age:(NSString *)age
                        state:(NSString *)state
                        zipcode:(NSString *)zipcode
                        gender:(NSString *)gender
                        birthdate:(NSString *)birthdate
                   completion:(CompletionHandler)completion {

    NSDictionary *parameters = @{
                                 @"action":@"clientRegistrationModal",
                                 @"currentController":@"store",
                                 @"single_page":@"2",
                                 @"first_name": firstName,
                                 @"last_name": lastName,
                                 @"contact_phone": contactPhone,
                                 @"email_address": emailAddress,
                                 @"password": password,
                                 @"cpassword": confirmPassword,
                                 @"age": age,
                                 @"state": state,
                                 @"zipcode": zipcode,
                                 @"gender": gender,
                                 @"birthdate": birthdate};
    
    [self perfomRequestWithPath:@"" parameters:parameters completion:^(BOOL success, id response, NSError *error) {
        if (success) {
            completion(YES, response, nil);
        } else {
            completion(NO, nil, error);
        }
    }];
}

- (void)loginUserWithUserName:(NSString *)emailAddress
                    password:(NSString *)password
                      completion:(CompletionHandler)completion {
    
    NSDictionary *parameters = @{
                                 @"action":@"clientLogin",
                                 @"currentController":@"store",
                                 @"username": emailAddress,
                                 @"password": password};
    
    [self perfomRequestWithPath:@"" parameters:parameters completion:^(BOOL success, id response, NSError *error) {
        if (success) {
            completion(YES, response, nil);
        } else {
            completion(NO, nil, error);
        }
    }];
}
    
- (void)facebookRegisterUserWithUserName:(NSString *)firstName
                        lastName:(NSString *)lastName
                    emailAddress:(NSString *)emailAddress
                        userID:(NSString *)userID
                          gender:(NSString *)gender
                      completion:(CompletionHandler)completion {
    
    NSDictionary *parameters = @{
                                 @"action":@"FBRegister",
                                 @"currentController":@"store",
                                 @"first_name": firstName,
                                 @"last_name": lastName,
                                 @"email": emailAddress,
                                 @"password": userID,
                                 @"gender": gender};
    
    [self perfomRequestWithPath:@"" parameters:parameters completion:^(BOOL success, id response, NSError *error) {
        if (success) {
            completion(YES, response, nil);
        } else {
            completion(NO, nil, error);
        }
    }];
}





















- (void)getRestaurantWithID:(NSString *)userID
                 completion:(CompletionHandler)completion {
    
//    NSString* path = [NSString stringWithFormat:@"%@%@", getRestaurant, userID];
//    
//    [self perfomRequestWithPath:path parameters:nil completion:^(BOOL success, id response, NSError *error) {
//        if (success) {
//            //NSLog(@"getRestaurantWithID response: %@", response);
//            
//            completion(YES, response, nil);
//        } else {
//            completion(NO, nil, error);
//        }
//    }];
}

- (void)getNewOrders:(NSString *)userID
          completion:(CompletionHandler)completion {
//    NSString* path = [NSString stringWithFormat:@"%@%@", getCompleteOrders, userID];
//    NSLog(@"%@", path);
//    [self perfomRequestWithPath:path parameters:nil completion:^(BOOL success, id response, NSError *error) {
//        if (success) {
//            //NSLog(@"getNewOrders response: %@",response);
//            
//            completion(YES, [Order getOrderListFromResponse:response[@"data"]], nil);
//        } else {
//            completion(NO, nil, error);
//        }
//    }];
}



@end
