//
//  MainMapViewController.h
//  Tipo
//
//  Created by Александр on 15.09.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MainMapViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
