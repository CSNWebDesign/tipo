//
//  RegistrationViewController.h
//  Tipo
//
//  Created by Александр on 15.09.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationViewController : UIViewController <UITextFieldDelegate>

@property (strong,nonatomic) NSDictionary* userInfo;

@end
