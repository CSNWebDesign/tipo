//
//  NetworkManagerConstants.h
//  Tipo
//
//  Created by Александр on 04.10.16.
//  Copyright © 2016 AlexandrBurla. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - baseURL

static NSString *const baseURLString = @"http://tipo.csnwebdesign.com/admin/ajax?";

static NSString *const imagesURLString = @"http://ec2-52-37-255-219.us-west-2.compute.amazonaws.com";

#pragma mark - Sign Up

static NSString *const login = @"/serve/3.0/auth/restaurant";
static NSString *const registration = @"clientRegistrationModal&currentController=store&single_page=2&first_name=test1&last_name=test1&contact_phone=%2B19315025244&email_address=test1%40gmail.com&password=12345&cpassword=12345";






static NSString *const getRestaurant = @"/serve/2.0/restaurant/";

#pragma mark - Orders

static NSString *const getCompleteOrders = @"/serve/3.0/orders/?restaurant_ids=";

